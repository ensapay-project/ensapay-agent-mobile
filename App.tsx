import "react-native-gesture-handler";
import React, {useEffect, useState} from "react";
import {StyleSheet} from "react-native";
import {LoginView} from "./views/login";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {authState, keycloak} from "./services/auth.service";
import {ComptesView} from "./views/home";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {KeycloakProvider, useKeycloak,} from 'expo-keycloak';
import {DarkTheme, NavigationContainer} from '@react-navigation/native';
import {ProfilView} from "./views/ProfilView";
import {Root} from "native-base";
import {ClientsView} from "./views/ClientsView";
import {applyMiddleware, createStore} from 'redux';
import {Provider} from 'react-redux';
import thunk from "redux-thunk";

import reducers from './redux/reducers';
import axios from "axios";
import rootReducer from "./redux/reducers";
import {clientsReducer} from "./redux/reducers/clients-reducer";


const Tab = createBottomTabNavigator();

export const store = createStore(rootReducer, applyMiddleware(thunk));

export default function App() {
    return (<KeycloakProvider
        {...keycloak}
        // authClient={keycloak}
        // onEvent={(event, error) => handleOnEvent(event, error)}
        // LoadingComponent={<Text>Loading</Text>}
    >
        <Provider store={store}>
            <Main/>
        </Provider>

    </KeycloakProvider>);
}

function Main() {

    const {
        ready, // If the discovery is already fetched
        login, // The login function - opens the browser
        isLoggedIn, // Helper boolean to use e.g. in your components down the tree
        token, // Access token, if available
        logout, // Logs the user out
    } = useKeycloak();
    const [authenticated, setAuthenticated] = useState(isLoggedIn);
    // const handleOnEvent = (event: any, error: any) => {
    //     if (event === 'onAuthSuccess') {
    //         if (keycloak.authenticated) {
    //             console.log("Authenticated", keycloak);
    //             setAuthenticated(true);
    //         }
    //     }
    // }
    // useEffect(() => {
    //     if (ready && !isLoggedIn)
    //         login();
    // }, [ready,isLoggedIn]);

    useEffect(() => {
            setAuthenticated(isLoggedIn);
            authState.token = token;
        // if (ready) {
        //     // axios/
        // }
    }, [isLoggedIn, token, ready]);

    const handleLogout = () => {
        logout();
    }

    return !authenticated ? <LoginView login={login}/> :
        // <SafeAreaView><Text onPress={handleLogout}>Logout</Text></SafeAreaView>
        <Root>
            <NavigationContainer >
                <Tab.Navigator>
                    <Tab.Screen
                        name="ClientsStack"
                        component={ClientsView}
                        options={{
                            tabBarLabel: "Clients",
                            tabBarIcon: ({color, size}) => (
                                <MaterialCommunityIcons name="home" color={color} size={size}/>
                            ),
                        }}
                    />
                    <Tab.Screen
                        name="ComptesStack"
                        component={ComptesView}
                        options={{
                            tabBarLabel: "Comptes",
                            tabBarIcon: ({color, size}) => (
                                <MaterialCommunityIcons
                                    name="wallet"
                                    color={color}
                                    size={size}
                                />
                            ),
                        }}
                    />
                    <Tab.Screen
                        name="ProfilStack"
                        component={ProfilView}
                        options={{
                            tabBarLabel: "Profil",
                            tabBarIcon: ({color, size}) => (
                                <MaterialCommunityIcons
                                    name="account"
                                    color={color}
                                    size={size}
                                />
                            ),
                        }}
                    />
                </Tab.Navigator>
            </NavigationContainer></Root>
        ;
    // (
    // <NavigationContainer>
    //   <Tab.Navigator>
    //     <Tab.Screen
    //       name="HomeStack"
    //       component={HomeView}
    //       options={{
    //         tabBarLabel: "Home",
    //         tabBarIcon: ({ color, size }) => (
    //           <MaterialCommunityIcons name="home" color={color} size={size} />
    //         ),
    //       }}
    //     />
    //     <Tab.Screen
    //       name="SettingsStack"
    //       component={ComptesView}
    //       options={{
    //         tabBarLabel: "Settings",
    //         tabBarIcon: ({ color, size }) => (
    //           <MaterialCommunityIcons
    //             name="settings"
    //             color={color}
    //             size={size}
    //           />
    //         ),
    //       }}
    //     />
    //   </Tab.Navigator>
    // </NavigationContainer>

    // {!authenticated? <LoginView/> :
    //     <NavigationContainer>
    //       <Tab.Navigator>
    //         <Tab.Screen
    //           name="ClientsStack"
    //           component={HomeView}
    //           options={{
    //             tabBarLabel: "Clients",
    //             tabBarIcon: ({ color, size }) => (
    //               <MaterialCommunityIcons name="home" color={color} size={size} />
    //             ),
    //           }}
    //         />
    //         <Tab.Screen
    //           name="ComptesStack"
    //           component={ComptesView}
    //           options={{
    //             tabBarLabel: "Comptes",
    //             tabBarIcon: ({ color, size }) => (
    //               <MaterialCommunityIcons
    //                 name="settings"
    //                 color={color}
    //                 size={size}
    //               />
    //             ),
    //           }}
    //         />
    //         <Tab.Screen
    //           name="ProfilStack"
    //           component={ProfilView}
    //           options={{
    //             tabBarLabel: "Profil",
    //             tabBarIcon: ({ color, size }) => (
    //               <MaterialCommunityIcons
    //                 name="avatar"
    //                 color={color}
    //                 size={size}
    //               />
    //             ),
    //           }}
    //         />
    //       </Tab.Navigator>
    //     </NavigationContainer>
    // }
    // <PaperProvider>
    //   <View style={styles.container}>
    //     <Text>Home Scresen</Text>
    //     {/* <Button>Hello world</Button> */}
    //     {/* <Button title="Hello world" onPress={() => alert("Hello")} /> */}
    //     <TextInput placeholder="Hello" />
    //   </View>
    // </PaperProvider>
    // <View style={styles.container}>
    //   <Image
    //     source={{ uri: "https://reactnative.dev/img/homepage/phones.png" }}
    //     style={{ width: 305, height: 159 }}
    //   />
    //   <Text>Open up App.ts to start working on your app!</Text>
    //   <TouchableOpacity
    //     onPress={() => alert("Hello, world!")}
    //     // style={{ backgroundColor: "blue" }}
    //   >
    //     <Text style={{ fontSize: 20 }}>Pick a photo</Text>
    //   </TouchableOpacity>
    //   <StatusBar style="auto" />
    // </View>
    // );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },
});

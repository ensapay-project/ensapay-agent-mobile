export interface Client {
    userId?: string;
    numeroTel?: string;
    account?: {
        firstName?: string;
        lastName?: string;
        email?: string;
    };
}

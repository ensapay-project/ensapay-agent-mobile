import {Client} from "../../entities/client";
import {Compte} from "../../entities/compte";
// import {ClientsAction} from "../reducers/clients-reducer";

export const addClients = (payload: Client[]) => ({
    type: 'GET_ALL_CLIENTS',
    payload
});


export const addClient = (payload: any) => {
    return ({
        type: 'ClientActions.CREATE',
        payload
    })
};


export const modifyClient = (payload: Compte) => {

    return ({
        type: `ClientActions.UPDATE`,
        payload
    })
};

export const deleteClient = (payload: String) => {
    return ({
        type: `ClientActions.DELETE`,
        payload
    })
};

//
// export const addClient = (payload: any) => {
//     return ({
//         type: 'ClientActions.CREATE',
//         payload
//     })
// };

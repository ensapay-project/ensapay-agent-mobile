import {CompteAction, CompteActions} from "../reducers/comptes-reducer";
import {Compte} from "../../entities/compte";
import {fetchComptes} from "../../services/comptes-service";
// import {ClientsAction} from "../reducers/clients-reducer";


export const getComptes: any = (payload: Compte[]) => {
    // alert("Gettings comptes ");
    return ({
        type: 'GET_ALL_COMPTES',
        payload
    })
};

export const addCompte = (payload: Compte) => {
    return ({
        type: 'CompteActions.CREATE',
        payload
    })
};

export const modifyCompte = (payload: Compte) => {
    console.log("compte updated");

    return ({
        type: `CompteActions.UPDATE`,
        payload
    })
};

export const deleteCompte = (payload: String) => {
    return ({
        type: `CompteActions.DELETE`,
        payload
    })
};

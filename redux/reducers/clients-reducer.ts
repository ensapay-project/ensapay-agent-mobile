import {Client} from "../../entities/client";
import {CompteAction, CompteActions} from "./comptes-reducer";
import {Compte} from "../../entities/compte";

export interface ClientsState {
    clients: Client[],
    comptes: Compte[]
}

const INITIAL_STATE: ClientsState = {
    clients: [],
    comptes: []
};


// export const ClientsAction = {
//     'GET_ALL_CLIENTS',
//     'GET_ALL_COMPTES',
//     'CREATE',
//     'UPDATE',
//     'DELETE'
// }

//
export interface ClientAction {
    type: string,
    payload?: any;
}

export const clientsReducer = (state = INITIAL_STATE, action: ClientAction) => {
    switch (action.type) {
        case 'GET_ALL_CLIENTS':
            return ({...state, clients: action.payload});

        case 'GET_ALL_COMPTES':
            return ({...state, comptes: action.payload});

        case 'ClientActions.CREATE':
            return ({...state, clients: [...state.clients, action.payload] });

        case `ClientActions.UPDATE`:
            const clients = state.clients;
            return ({
                ...state, clients: clients.map(client =>
                    client.userId === action.payload.userId ? action.payload : client
                )
            });

        case `ClientActions.DELETE`:
            return ({...state, clients: state.clients.filter(client => client.userId !== action.payload)});

        case 'CompteActions.CREATE':
            return ({...state, comptes: [...state.comptes, action.payload]});
        //
        case `CompteActions.UPDATE`:
            const comptes = state.comptes;
            // comptes[comptes.findIndex(compte => compte.numeroCompte === action.payload.numeroCompte)] = action.payload;
            return ({
                ...state, comptes: comptes.map(compte =>
                    compte.numeroCompte === action.payload.numeroCompte ? action.payload : compte
                )
            });
        //
        case `CompteActions.DELETE`:
            return ({...state, comptes: state.comptes.filter(compte => compte.numeroCompte !== action.payload)});
        default:
            return state;
    }
}

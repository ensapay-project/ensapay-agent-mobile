import {Compte} from "../../entities/compte";

export interface ComptesState {
    comptes: Compte[];
}

const INTIAL_STATE: ComptesState = {
    comptes: []
};

export enum CompteActions {
    GET_ALL_COMPTES,
    CREATE,
    UPDATE,
    DELETE
}

export interface CompteAction {
    type: CompteActions,
    payload?: any;
}

export const comptesReducer = (state = INTIAL_STATE, action: CompteAction) => {
    switch (action.type) {
        case CompteActions.GET_ALL:
            return ({...state, comptes: action.payload});

        case CompteActions.CREATE:
            return ({...state, comptes: [...state.comptes, action.payload]});

        case CompteActions.UPDATE:
            const comptes = state.comptes;
            comptes[comptes.findIndex(compte => compte.numeroCompte === action.payload.numeroCompte)] = action.payload;
            return ({...state, comptes: comptes.map(compte =>
                compte.numeroCompte === action.payload.numeroCompte? action.payload : compte
                )});

        case CompteActions.DELETE:
            return ({...state, comptes: state.comptes.filter(compte => compte.numeroCompte !== action.payload)});
        default:
            return state;

    }
};

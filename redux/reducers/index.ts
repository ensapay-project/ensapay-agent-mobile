import { combineReducers } from 'redux';
import {clientsReducer} from "./clients-reducer";

// export type ReduxAction<T> = (payload?: any) => CompteAction<T>;

const rootReducer = combineReducers({
    // comptes: comptesReducer,
    clients: clientsReducer
});

export default rootReducer;

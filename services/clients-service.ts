import axiosInstance from "../request.interceptor";
import {config} from "../constants/api";
import {addClient, addClients, deleteClient, modifyClient} from "../redux/actions/client-actions";
import {Compte} from "../entities/compte";
import {deleteCompte, modifyCompte} from "../redux/actions/compte-actions";
import {Client} from "../entities/client";

export const fetchClients = () => (dispatch: any) =>
    axiosInstance.get(`${config.API_URL}/clients`)
        .then(({data}) => dispatch(addClients(data)));

export const createClient = (payload: any) => (dispatch: any) =>
    axiosInstance.post(`${config.API_URL}/clients`, payload)
        .then(({data}) => dispatch(addClient(data)));

export const updateClient = (id: string, client: Client) => (dispatch: any) =>
    axiosInstance.put(`${config.API_URL}/clients/${id}`, client)
        .then(({data}) => {
            console.log("updated client", data);
            dispatch(modifyClient(data));
        });

export const resetPassword = (client: Client) =>
    axiosInstance.post(`${config.API_URL}/clients/reset-password`, client.account);

export const removeClient = (id: string) => (dispatch: any) =>
    axiosInstance.delete(`${config.API_URL}/clients/${id}`)
        .then(() => {
            dispatch(deleteClient(id));
        });

import {config} from "../constants/api";
import {addCompte, deleteCompte, getComptes, modifyCompte} from "../redux/actions/compte-actions";
import axiosInstance from "../request.interceptor";
import {Compte} from "../entities/compte";

export const fetchComptes = () => (dispatch: any) =>
    axiosInstance.get(`${config.API_URL}/comptes`)
        .then(({data}) => {
            // console.log("Fetched comptes", data);
            dispatch(getComptes(data));
        });

export const createCompte = (compte: Compte) => (dispatch: any) =>
    axiosInstance.post(`${config.API_URL}/comptes`, compte)
        .then(({data}) => {
            console.log("created comptes", data);
            dispatch(addCompte(data));
        });

export const updateCompte = (id: string, compte: Compte) => (dispatch: any) =>
    axiosInstance.put(`${config.API_URL}/comptes/${id}`, compte)
        .then(({data}) => {
            console.log("updated comptes", data);
            dispatch(modifyCompte(data));
        });

export const removeCompte = (id: string) => (dispatch: any) =>
    axiosInstance.delete(`${config.API_URL}/comptes/${id}`)
        .then(() => {
            dispatch(deleteCompte(id));
        });
// .then(response => {
//     console.log("Fetched comptes", response.data);
// });

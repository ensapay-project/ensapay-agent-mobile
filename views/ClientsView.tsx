import React, {useEffect, useState} from "react";
import {
    ActionSheet,
    Body,
    Button,
    Container,
    Content,
    Form,
    Icon,
    Input,
    Item,
    Label,
    Left,
    List,
    ListItem,
    Right,
    Subtitle,
    Text, Thumbnail,
    Title
} from "native-base";
import {createStackNavigator} from "@react-navigation/stack";
import {Client} from "../entities/client";
import {Div} from "react-native-magnus";
import {connect} from "react-redux";
import {createClient, fetchClients, removeClient, resetPassword, updateClient} from "../services/clients-service";
import {ActivityIndicator, FlatList, RefreshControl, ScrollView, TouchableOpacity} from "react-native";
import {Compte} from "../entities/compte";

const Stack = createStackNavigator();

export const ClientsView = (props: any) => {
    return (
        <Stack.Navigator
            initialRouteName="Home"
            screenOptions={{
                headerStyle: {backgroundColor: "#2196f3"},
                headerTintColor: "#fff",
                headerTitleStyle: {fontWeight: "bold"},
            }}

        >
            <Stack.Screen
                name="Home"
                component={ClientsListState}
                options={{title: "List de clients",
                    // headerRight: props => <TouchableOpacity  ><Text>Ajouter</Text></TouchableOpacity>
                }}

            />
            <Stack.Screen
                name="ClientDetails"
                component={ClientDetailsState}
                options={{title: "Details de client",}}
                // initialParams={}
            />
            <Stack.Screen
                name="ClientForm"
                component={ClientFormState}
                options={{title: "Créer un client",}}
                // initialParams={}
            />
        </Stack.Navigator>
    );
};

const ClientsList = ({navigation, clients, fetchClients}: any) => {
    // const [clients, setClients] = useState([
    //     {
    //         id: "XXX_XX",
    //         name: "Oussama",
    //         email: "o@g.c"
    //     },
    //     {
    //         id: "ZZZZ",
    //         name: "Khalil",
    //         email: "kh@g.c"
    //     },
    // ] as Client[]);

    const [refreshing, setRefreshing] = useState(false);

    // const refresh = async () => {
    //     setRefreshing(true);
    //     await fetchClients();
    //     setRefreshing(false);
    // }

    const onRefresh = React.useCallback(async () => {
            setRefreshing(true);
            await fetchClients();
            setRefreshing(false);
        },[refreshing]);

    useEffect(
        () => {
            fetchClients();
            console.log("Fetched clients", clients);
        }, []);


    return <ScrollView  refreshControl={<RefreshControl
        refreshing={refreshing}
        onRefresh={onRefresh}
    />}>
        <Container>
        <Content padder>
            <Button style={{width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center'}} primary
                    onPress={() => navigation.navigate('ClientForm',)}>
                <Text style={{textAlign: 'center'}}>
                    Ajouter
                </Text>
            </Button>
            <Div style={{height: 20}}/>
            {/*<Text>{JSON.stringify(clients)}</Text>*/}
            {clients && clients.length === 0 && <Text style={{textAlign: 'center'}}>
                Aucun client ajouté.
            </Text>}
            {/*<FlatList*/}
            {/*    data={clients}*/}
            {/*    keyExtractor={(item) => item.numeroCompte}*/}
            {/*    renderItem={*/}
            {/*        ({item}) => <Item */}
            {/*                                                   onPress={() => navigation.navigate('ClientDetails', {*/}
            {/*                                                       item,*/}
            {/*                                                       navigate: navigation.navigate*/}
            {/*                                                   })}>*/}
            {/*            <Left>*/}
            {/*                <Text>{item.account?.firstName} {item.account?.lastName}</Text>*/}
            {/*            </Left>*/}
            {/*            <Right>*/}
            {/*                <Icon name="arrow-forward"/>*/}
            {/*            </Right>*/}
            {/*        </Item>*/}
            {/*    }*/}
            {/*    refreshControl={<RefreshControl*/}
            {/*    refreshing={refreshing}*/}
            {/*    onRefresh={onRefresh}*/}
            {/*/>}/>*/}

            <List>
                {clients && clients.map((client: Client, key: number) => <ListItem  key={key}
                                                                                   onPress={() => navigation.navigate('ClientDetails', {
                                                                                       client,
                                                                                       navigate: navigation.navigate
                                                                                   })}>
                        {/*<Left>*/}
                        {/*    <Thumbnail source={{ uri: 'Image URL' }} />*/}
                        {/*</Left>*/}
                            <Body>
                            <Text>{client.account?.firstName} {client.account?.lastName}</Text>
                                {/*<Text>Kumar Pratik</Text>*/}
                                <Text note>{client.numeroTel} • {client.account?.email}</Text>
                            </Body>
                        <Right>
                            <Icon name="arrow-forward"/>
                        </Right>
                    </ListItem>
                )}
            </List>
        </Content>
    </Container>
    </ScrollView>

}

const mapStateToClientsListProps = (state: any) => ({
    clients: state.clients.clients
});

export const ClientsListState = connect(mapStateToClientsListProps, {fetchClients})(ClientsList);

const ClientDetails = (props: any) => {
    const client = props.route.params.client as Client;

    const [sending, setSending] = useState(false);

    return (<Container>
        <Content padder>
            {/*<Body >*/}
            <Subtitle style={{fontSize: 18, textAlign: "left"}}>Nom de client</Subtitle>
            <Title
                style={{fontSize: 36, textAlign: "left"}}>{client.account?.firstName} {client.account?.lastName}</Title>
            <Div style={{height: 20}}/>
            <Subtitle style={{fontSize: 18, textAlign: "left"}}>Email</Subtitle>
            <Title style={{fontSize: 36, textAlign: "left"}}>{client.account?.email}</Title>
            <Div style={{height: 20}}/>
            <Subtitle style={{fontSize: 18, textAlign: "left"}}>Numero de telephone</Subtitle>
            <Title style={{fontSize: 36, textAlign: "left"}}>{client.numeroTel}</Title>
            <Div style={{height: 20}}/>
            <Button style={{width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center'}} primary
                    onPress={() => props.route.params.navigate('ClientForm', {client,})}>
                <Text style={{textAlign: 'center'}}>
                    Modifier
                </Text>
            </Button>
            <Div style={{height: 10}}/>

            {!sending ?
                <Button style={{width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center'}} primary
                        onPress={() => {
                            ActionSheet.show(
                                {
                                    options: ["Continuer", "Annuler"],
                                    cancelButtonIndex: 1,
                                    destructiveButtonIndex: 0,
                                    title: "Si vous continuez, le client ne peut plus se connecter avec son mot de passe actuel."
                                },
                                buttonIndex => {
                                    if (buttonIndex == 0) {
                                        setSending(true);
                                        resetPassword(client)
                                            .then(() => {
                                                setSending(false);
                                                alert("Un nouveau mot de passe a été au client");
                                            });
                                    }
                                    // props.route.params.navigate('Home',);};

                                }
                            );
                        }
                        }>
                    <Text style={{textAlign: 'center'}}>
                        Envoyer un nouveau mot de passe
                    </Text>
                </Button> : <ActivityIndicator/>}
            <Div style={{height: 10}}/>
            <Button style={{width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center'}} danger
                    onPress={() => {
                        ActionSheet.show(
                            {
                                options: ["Confirmer", "Annuler"],
                                cancelButtonIndex: 1,
                                destructiveButtonIndex: 0,
                                title: "La suppression de ce client entrainera la suppression de toutes ses données et ses comptes. Voulez-vous continuer?"
                            },
                            buttonIndex => {
                                if (buttonIndex == 0) {
                                    props.removeClient(client.userId)
                                        .then(() => props.route.params.navigate('Home',));
                                }
                                // props.route.params.navigate('Home',);};

                            }
                        );
                    }
                    }>
                <Text style={{textAlign: 'center'}}>
                    Supprimer
                    {/*{JSON.stringify(props.route.params)}*/}
                </Text>
            </Button>

        </Content></Container>);
}

export const ClientDetailsState = connect(null, {removeClient})(ClientDetails);

const ClientForm = (props: any) => {
    const [client, setClient] = useState(props.route?.params?.client || {} as Client);

    const [saving, setSaving] = useState(false);
    return <Container>
        <Content padder>
            <Form>
                <Item fixedLabel>
                    <Label>Nom de famille</Label>
                    <Input value={client.account?.lastName}
                           onChangeText={lastName => setClient({...client, account: {...client.account, lastName}})}
                           textContentType="familyName"/>
                </Item>
                <Item fixedLabel>
                    <Label>Prenom</Label>
                    <Input value={client.account?.firstName}
                           onChangeText={firstName => setClient({...client, account: {...client.account, firstName}})}
                           textContentType="familyName"/>
                </Item>
                <Item fixedLabel>
                    <Label>Email</Label>
                    <Input value={client.account?.email}
                           onChangeText={email => setClient({...client, account: {...client.account, email}})}
                           keyboardType="email-address" textContentType={"emailAddress"}/>
                </Item>
                <Item fixedLabel>
                    <Label>Numero de tel</Label>
                    <Input value={client.numeroTel} onChangeText={numeroTel => setClient({...client, numeroTel})}
                           keyboardType="phone-pad"/>
                </Item>
            </Form>
            <Div style={{height: 20}}/>
            <Button style={{width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center'}} primary
                    onPress={() => {
                        setSaving(true);
                        const requestBody = {
                            ...client,
                            email: client.account?.email,
                            firstName: client.account.firstName,
                            lastName: client.account.lastName
                        };
                        if (!client.userId) {
                            props.createClient(requestBody)
                                .then(() => props.navigation.navigate('Home',));
                        } else {
                            props.updateClient(client.userId, requestBody)
                                .then(() => props.navigation.navigate('ClientDetails', {client}));
                        }
                    }}>
                {saving ? <ActivityIndicator/> : <Text style={{textAlign: 'center'}}>
                    Enregistrer
                </Text>}
            </Button>
        </Content>
    </Container>
}

const ClientFormState = connect(null, {createClient, updateClient})(ClientForm);

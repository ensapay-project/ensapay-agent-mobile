import React, {useEffect, useState} from "react";
import {createStackNavigator} from "@react-navigation/stack";
import {loadUserInfo} from "../services/auth.service";
import {ActionSheet, Button, Container, Content, ListItem, Subtitle, Text, Title} from 'native-base';
import {useKeycloak} from "expo-keycloak";
import {Div} from "react-native-magnus";
import {StyleSheet} from "react-native";
import {config} from "../constants/api";
import * as Linking from 'expo-linking';
import * as WebBrowser from 'expo-web-browser';

const Stack = createStackNavigator();

const ProfilPage = (props: any) => {
    const [currentUser, setCurrentUser] = useState({} as any);
    const {
        ready, // If the discovery is already fetched
        login, // The login function - opens the browser
        isLoggedIn, // Helper boolean to use e.g. in your components down the tree
        token, // Access token, if available
        logout, // Logs the user out
    } = useKeycloak();
    useEffect(() => {
            if (ready)
                loadUserInfo(token)
                    .then(response => setCurrentUser(response.data))
            // keycloak.loadUserInfo().then(console.log);
        }
        , [ready]);

    return (<Container>
        <Content padder>
            {/*<Body >*/}
            <Subtitle style={{ fontSize: 18, textAlign: "left" }}>Vous connecte en tant que</Subtitle>
            <Title style={{ fontSize: 36, textAlign: "left" }}>{currentUser.name}</Title>
            <Div style={{ height: 20 }} />
            <Subtitle style={{ fontSize: 18, textAlign: "left" }}>Email</Subtitle>
            <Title style={{ fontSize: 36, textAlign: "left" }}>{currentUser.email}</Title>
            <Div style={{ height: 20 }} />
            <Button style={{ width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }} primary
            onPress={() => WebBrowser.openBrowserAsync(`${config.AUTH_SERVER}/realms/ensapay/account`)}
            >
                <Text style={{ textAlign: 'center' }}>
                    Modifier vos informations
                </Text>
            </Button>
            {/*<Div style={{ height: 10 }} />*/}
            {/*<Button style={{ width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }} primary >*/}
            {/*    <Text style={{ textAlign: 'center' }}>*/}
            {/*        Modifier vos mot de passe*/}
            {/*    </Text>*/}
            {/*</Button>*/}
            <Div style={{ height: 10 }} />
            {/*<Div style={{ height: '100%' }} />*/}

            <Button style={{ marginTop: "auto",width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }} danger  onPress={() => {
                ActionSheet.show(
                    {
                        options: ["Continuer", "Annuler"],
                        cancelButtonIndex: 1,
                        destructiveButtonIndex: 0,
                        title: "Se déconnecter"
                    },
                    buttonIndex => {
                        if (buttonIndex == 0) logout();
                    }
                );}
            }>
                <Text style={{ textAlign: 'center' }}>
                    Deconnexion
                    {/*{JSON.stringify(props.route.params)}*/}
                </Text>
            </Button>

        </Content></Container>);
}

export const ProfilView = (props: any) => {

    return (
        <Stack.Navigator
            initialRouteName="profil"
            screenOptions={{
                headerStyle: {backgroundColor: "#2196f3"},
                headerTintColor: "#fff",
                headerTitleStyle: {fontWeight: "bold"},
            }}
        >
            <Stack.Screen
                name="profil"
                component={ProfilPage}
                options={{title: "Votre profilos"}}
            />
        </Stack.Navigator>
    );
}

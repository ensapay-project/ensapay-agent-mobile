import React, {useEffect, useState} from "react";
import {
    ActivityIndicator,
    FlatList,
    ListView,
    RefreshControl,
    SafeAreaView,
    ScrollView,
    StyleSheet,
} from "react-native";
import {createStackNavigator} from "@react-navigation/stack";
import {
    ActionSheet,
    Body,
    Button,
    Container,
    Content,
    Form,
    Icon,
    Input,
    Item,
    Label,
    Left,
    List,
    ListItem,
    Picker,
    Right,
    Subtitle,
    Text,
    Title
} from "native-base";
import {Compte} from "../entities/compte";
import {Div} from "react-native-magnus";
import {ClientsView} from "./ClientsView";
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {deleteCompte, getComptes} from "../redux/actions/compte-actions";
import {createCompte, fetchComptes, removeCompte, updateCompte} from "../services/comptes-service";
import {fetchClients} from "../services/clients-service";
import {Client} from "../entities/client";

const Stack = createStackNavigator();

export const HomeView = (props: any) => {

    return (
        <Stack.Navigator
            initialRouteName="Home"
            screenOptions={{
                headerStyle: {backgroundColor: "#2196f3"},
                headerTintColor: "#fff",
                headerTitleStyle: {fontWeight: "bold"},
            }}
        >
            <Stack.Screen
                name="Home"
                component={ClientsView}
                options={{title: "Gestion de clients"}}
            />
            <Stack.Screen
                name="Details"
                component={Comptes}
                options={{title: "Comptes Page"}}
            />
        </Stack.Navigator>
    );
};

export const ComptesView = (props: any) => {
    return (
        <Stack.Navigator
            initialRouteName="Home"
            screenOptions={{
                headerStyle: {backgroundColor: "#2196f3"},
                headerTintColor: "#fff",
                headerTitleStyle: {fontWeight: "bold"},
            }}
        >
            <Stack.Screen
                name="Home"
                component={ComptesViewState}
                options={{title: "Gestion de comptes",}}
            />
            <Stack.Screen
                name="Details"
                component={DetailsState}
                options={{title: "Details de compte",}}
                // initialParams={}
            />
            <Stack.Screen
                name="CompteForm"
                component={CompteFormState}
                options={{title: "Créer un compte",}}
                // initialParams={}
            />
        </Stack.Navigator>
    );
};


export const Details = (props: any) => {
    const compte = props.route.params.compte;
    console.log(props);

    return (<Container>
        <Content padder>
            {/*<Body >*/}
            <Subtitle style={{fontSize: 18, textAlign: "left"}}>Numero de compte</Subtitle>
            <Title style={{fontSize: 36, textAlign: "left"}}>{compte.numeroCompte}</Title>
            <Div style={{height: 20}}/>
            <Subtitle style={{fontSize: 18, textAlign: "left"}}>Intitule</Subtitle>
            <Title style={{fontSize: 36, textAlign: "left"}}>{compte.intitule}</Title>
            <Div style={{height: 20}}/>
            <Subtitle style={{fontSize: 18, textAlign: "left"}}>Solde</Subtitle>
            <Title style={{fontSize: 36, textAlign: "left"}}>{compte.soldeFinal} DH</Title>
            <Div style={{height: 20}}/>
            <Button style={{width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center'}} primary
                    onPress={() => props.route.params.navigate('CompteForm', {compte,})}>
                <Text style={{textAlign: 'center'}}>
                    Modifier
                </Text>
            </Button>
            <Div style={{height: 10}}/>
            <Button style={{width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center'}} danger
                    onPress={() => {
                        ActionSheet.show(
                            {
                                options: ["Supprimer", "Annuler"],
                                cancelButtonIndex: 1,
                                destructiveButtonIndex: 0,
                                title: "Confirmer la suppression"
                            },
                            buttonIndex => {
                                if (buttonIndex == 0)
                                    props.removeCompte(compte.numeroCompte)
                                        .then(() => props.route.params.navigate('Home',));

                                props.route.params.navigate('Home',);
                            }
                        );
                    }
                    }>
                <Text style={{textAlign: 'center'}}>
                    Supprimer
                    {/*{JSON.stringify(props.route.params)}*/}
                </Text>
            </Button>
            {/*<Text style={{ textAlign: 'center' }}>*/}
            {/*    {JSON.stringify(props.route.params)}*/}
            {/*</Text>*/}
            {/*</Body>*/}
        </Content></Container>);
};
const mapDispatchToComptesDetailsProps = (dispatch: any) => (
    bindActionCreators({
        deleteCompte,
    }, dispatch)
);
const DetailsState = connect(null, {removeCompte})(Details);

const Comptes = ({navigation, comptes, fetchComptes, fetchClients}: any) => {

    useEffect(
        () => {
            fetchComptes();
            console.log("LIST", comptes);
        }, []);

    const [refreshing, setRefreshing] = useState(false);

    const onRefresh = React.useCallback(async () => {
        setRefreshing(true);
        await fetchClients();
        setRefreshing(false);
    }, [refreshing]);

    return <ScrollView refreshControl={<RefreshControl
        refreshing={refreshing}
        onRefresh={onRefresh}
    />}>
        <Container>
        <Content padder>
            <Button style={{width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center'}} primary
                    onPress={() => navigation.navigate('CompteForm',)}>
                <Text style={{textAlign: 'center'}}>
                    Ajouter
                </Text>
            </Button>
            <Div style={{height: 20}}/>
            {/*<Text>{JSON.stringify(comptes)}</Text>*/}
            {comptes && comptes.length === 0 && <Text style={{textAlign: 'center'}}>
                Aucun compte ajouté.
            </Text>}
            <List>
                {comptes && comptes.map((compte: Compte, key: number) => <ListItem
                                                                                   key={key}
                                                                                   onPress={() => navigation.navigate('Details', {
                                                                                       compte,
                                                                                       navigate: navigation.navigate
                                                                                   })}>
                        {/*<Left>*/}
                        {/*    /!*</>*!/*/}
                        {/*    /!*<Text>{"\n" + compte.intitule}</Text>*!/*/}
                        {/*</Left>*/}
                            <Body>
                                <Text>{compte.numeroCompte}</Text>
                                <Text note>{compte.intitule} • {compte.soldeFinal} DH</Text>

                            </Body>
                        <Right>
                            {/*<Button transparent>*/}
                            {/*    <Text>View</Text>*/}
                            {/*</Button>*/}
                            {/*<Body>*/}
                            <Icon name="arrow-forward"/>
                            {/*    /!*<Text>{compte.numeroCompte}</Text>*!/*/}
                            {/*    /!*<Text note>{compte.intitule}</Text>*!/*/}

                            {/*</Body>*/}
                        </Right>
                    </ListItem>
                )}


                {/*<ListItem>*/}
                {/*    <Text>Nathaniel Clyne</Text>*/}
                {/*</ListItem>*/}
                {/*<ListItem>*/}
                {/*    <Text>{JSON.stringify(comptes)}</Text>*/}
                {/*</ListItem>*/}
            </List>
        </Content>
        </Container>
    </ScrollView>
}

const mapStateToComptesProps = (state: any) => ({
    comptes: state.clients.comptes
});

const mapDispatchToComptesProps = (dispatch: any) => (
    bindActionCreators({
        getComptes,
    }, dispatch)
);

const ComptesViewState = connect(mapStateToComptesProps, {fetchComptes, fetchClients})(Comptes);

export const CompteForm = (props: any) => {

    useEffect(() => {
        props.fetchClients();
        // props.fetchComptes();
    }, [])

    const [sending, setSending] = useState(false);

    const [compte, setCompte] = useState(props.route?.params?.compte || {} as Compte);
    const [client, setClient] = useState({account: {firstName: "O"}} as Client);

    const handleSelectClient = (userId: string) => {
        alert(JSON.stringify(userId));
        const selectedClient = props.clients?.filter((client: Client) => client.userId === userId)[0];
        setClient(selectedClient);
        console.log("Sclients", props.clients);
        console.log("elected client", selectedClient);
    }

    return <Container>
        <Content padder>
            <Form>
                <Item fixedLabel>
                    <Label>Intitule</Label>
                    <Input value={compte.intitule} onChangeText={intitule => setCompte({...compte, intitule})}
                           textContentType="familyName"/>
                </Item>
                <Item fixedLabel>
                    <Label>Solde</Label>
                    <Input value={compte.soldeFinal?.toString()} onChangeText={solde => setCompte({
                        ...compte,
                        soldeFinal: Number.parseFloat(solde)
                    })}
                           keyboardType="numeric"/>
                </Item>
                <Div style={{height: 20}}/>
                {!compte.numeroCompte && <Item picker>
                    <Picker
                        mode="dropdown"
                        iosIcon={<Icon name="arrow-down"/>}
                        style={{width: undefined}}
                        placeholder="Selectionner un client"
                        placeholderStyle={{color: "#bfc6ea"}}
                        placeholderIconColor="#007aff"
                        selectedValue={client.userId}
                        onValueChange={handleSelectClient}

                        // onValueChange={this.onValueChange2.bind(this)}
                    >
                        {/*<Picker.Item label="Wallet" value={value} />*/}
                        {props.clients &&
                        props.clients.map((client: Client) =>
                            <Picker.Item label={client.account?.firstName ?? ''} value={client.userId ?? ''}/>
                        )
                        }
                    </Picker>
                </Item>}
            </Form>
            {!sending?<Button style={{width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center'}} primary
                    onPress={() => {
                        setSending(true);
                        console.log(compte);
                        if (compte.numeroCompte) {
                            props.updateCompte(compte.numeroCompte, compte)
                                .then(() => props.navigation.navigate('Details', {compte}));
                            // props.navigation.navigate('Details', {compte});
                        } else {
                            props.createCompte({...compte, clientId: client.userId})
                                .then(() => props.navigation.navigate('Home',));
                            // props.navigation.navigate('Home',);
                        }
                    }}>
                <Text style={{textAlign: 'center'}}>
                    Enregistrer
                </Text>
            </Button>: <ActivityIndicator/>}
        </Content>
    </Container>
}

const mapDispatchToCompteFormProps = (dispatch: any) => (
    bindActionCreators({
        createCompte,
        updateCompte
    }, dispatch)
);

const mapStateToComptesFormProps = (state: any) => ({
    clients: state.clients.clients,
    // comptes: state.comptes.comptes,
    // return {clients: clients.clients,};
});

const CompteFormState = connect(mapStateToComptesFormProps, {
    createCompte,
    updateCompte,
    fetchClients,
    fetchComptes
})(CompteForm);

const HomeScreen = ({navigation}: any) => {
    return (
        <SafeAreaView style={{flex: 1}}>
            <FlatList
                data={["Oussama", "Lahmidi"]}
                renderItem={({item}) => (
                    <Text style={styles.item} onPress={() => alert("Hello " + item)}>
                        {item}
                    </Text>
                )}
            />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    button: {
        alignItems: "center",
        backgroundColor: "#DDDDDD",
        padding: 10,
        width: 300,
        marginTop: 16,
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },
});
export default HomeScreen;

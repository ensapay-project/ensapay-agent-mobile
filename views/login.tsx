import { useKeycloak } from "expo-keycloak";
import React, { useState } from "react";
import { Text, View, StyleSheet, TextInput, Button, ActivityIndicator } from "react-native";
import { Div } from "react-native-magnus";
import {keycloak, login} from "../services/auth.service";
import {AuthSession} from "expo/build/removed.web";

const initialState = {
  username: "",
  password: "",
};

export const LoginView = (props: any) => {
  const [credentials, setCredentials] = useState(initialState);
    const {
        ready, // If the discovery is already fetched
        login, // The login function - opens the browser
        isLoggedIn, // Helper boolean to use e.g. in your components down the tree
        token, // Access token, if available
        logout, // Logs the user out
    } = useKeycloak();
    if (!ready) return <ActivityIndicator />;
  const handleLogin = async () => {
      props.login();
      // const redirectUrl = AuthSession.getRedirectUrl();
      // const result = await AuthSession.startAsync({
      //     authUrl:
      //         `https://www.linkedin.com/oauth/v2/authorization` +
      //         `?response_type=code` +
      //         `&client_id=ressource-server` +
      //         `&redirect_uri=${encodeURIComponent(redirectUrl)}` +
      //         '&scope=email%20profil'
      // });
    // console.log(credentials);
    // // login(credentials.username, credentials.password);
    //   if (ready)
    //     login().then((r: any) => alert(JSON.stringify(token)));
    // keycloak.login()
    //     .then(r => console.log(r));
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Welcome to ENSAPAY</Text>
      <Div style={{ height: 20 }}></Div>
      {/*<Text style={styles.subtitle}>*/}
      {/*  Entrez vos informations pour se connecter.*/}
      {/*</Text>*/}
      {/*<Div style={{ height: 20 }}></Div>*/}
      {/*<TextInput*/}
      {/*  style={styles.inputField}*/}
      {/*  placeholder="Username"*/}
      {/*  onChangeText={(username) =>*/}
      {/*    setCredentials({ ...credentials, username })*/}
      {/*  }*/}
      {/*/>*/}
      {/*<TextInput*/}
      {/*  style={styles.inputField}*/}
      {/*  placeholder="Password"*/}
      {/*  secureTextEntry*/}
      {/*  onChangeText={(password) =>*/}
      {/*    setCredentials({ ...credentials, password })*/}
      {/*  }*/}
      {/*/>*/}
      {/*<Div style={{ height: 10 }}></Div>*/}
      <Button title="Se connecter" onPress={handleLogin}>
        Hello world
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    fontWeight: "bold",
  },
  subtitle: {
    fontSize: 16,
    opacity: 0.7,
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  inputField: {
    width: "80%",
    backgroundColor: "#eee",
    borderRadius: 5,
    height: 50,
    marginBottom: 10,
    justifyContent: "center",
    padding: 20,
    fontSize: 18,
  },
  mainButton: {
    width: "80%",
    backgroundColor: "blue",
    borderRadius: 5,
    height: 50,
    marginBottom: 10,
    justifyContent: "center",
    padding: 20,
  },
});
